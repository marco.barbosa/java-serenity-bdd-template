package br.com.serenitybddtemplate.stepdefinitions;

import br.com.serenitybddtemplate.pages.AccountProfMenuPage;
import br.com.serenitybddtemplate.steps.*;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;

public class PerfilStepDefinition {
    String plataforma = "Desktop";
    String os = "Windows 10 Home Single Language";
    String version = "1.0.7";
    String perfil = plataforma + " " + os + " " + version;

    @Steps
    LoginSteps loginSteps;

    @Steps
    MainSteps mainSteps;

    @Steps
    AccountSteps accountSteps;

    @Steps
    AccountProfMenuSteps accountProfMenuSteps;

    @Given("vou até 'minha conta'")
    public void clicarEmMinhaConta() { mainSteps.clicarEmMyAccount(); }

    @And("entro em 'perfís'")
    public void clicarEmPerfis() { accountSteps.clicarEmPerfis(); }

    @And("preencho o formulário do perfil")
    public void preencherPerfil() {
        accountProfMenuSteps.preencherPlataforma(plataforma);
        accountProfMenuSteps.preencherSistemaOperacional(os);
        accountProfMenuSteps.preencherVersaoSistemaOperacional(version);
    }
    @And("seleciono o perfil")
    public void selecionarPerfil() { accountProfMenuSteps.selecionarPerfil(perfil);}

    @And("seleciono 'Apagar Perfil'")
    public void selecionarApagarPerfil() { accountProfMenuSteps.clicarEmApagarPerfil(); }

    @When("envio o formulário do perfil")
    public void clicarEmEnviarPerfil() {
        accountProfMenuSteps.clicarEmAdicionarPerfil();
    }

    @When("envio o formulário de excluir")
    public void clicarEmExcluirPerfil() { accountProfMenuSteps.excluirPerfil(); }

    @Then("o perfil deve ser criado com sucesso")
    public void verificaSeOPerfilFoiCriado() {
        Assert.assertEquals(accountProfMenuSteps.verificarSePerfilExiste(perfil), true);
    }

    @Then("o perfil deve ser excluido com sucesso")
    public void verificarSePerfilFoiExcluido() {
        Assert.assertEquals(accountProfMenuSteps.verificarSePerfilExiste(perfil), false);
    }
}
