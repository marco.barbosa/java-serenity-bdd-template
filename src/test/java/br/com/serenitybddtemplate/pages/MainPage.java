package br.com.serenitybddtemplate.pages;

import br.com.serenitybddtemplate.bases.PageBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

public class MainPage extends PageBase {

    //Constructor
    public MainPage(WebDriver driver){
        super(driver);
    }
    //Mapping
    By usernameLoginInfoTextArea = By.xpath("//td[@class='login-info-left']/span[@class='italic']");
    By reportIssueLink = By.xpath("//a[@href='/bug_report_page.php']");
    By myAccountLink = By.xpath("//a[@href='/account_page.php']");

    //Actions
    public String retornaUsernameDasInformacoesDeLogin(){
        return getText(usernameLoginInfoTextArea);
    }

    public void clicarEmReportIssue(){
        click(reportIssueLink);
    }

    public void clicarEmMyAccount(){
        click(myAccountLink);
    }

    public boolean VerificarSeLinkExiste(String link)
    {
        try
        {
            By getLink = By.xpath("//a[@href='"+link+"']");
            waitForElement(getLink);
            return true;
        }
        catch (WebDriverException e)
        {
            return false;
        }

    }
}
