package br.com.serenitybddtemplate.pages;

import br.com.serenitybddtemplate.bases.PageBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

public class AccountProfMenuPage extends PageBase {
    //Constructor
    public AccountProfMenuPage(WebDriver driver){
        super(driver);
    }

    //Mapping
    By platform = By.name("platform");
    By operationalSystem = By.name("os");
    By osBuild = By.name("os_build");
    By sendProfile = By.xpath("//input[@value='Adicionar Perfil']");
    By exclude = By.xpath("//input[@value='Enviar']");
    By deleteProfile = By.xpath("//input[@value='delete']");
    By profileComboBox = By.name("profile_id");

    //Actions
    public void preencherPlataforma(String plataforma){
        sendKeys(platform, plataforma);
    }

    public void preencherSistemaOperacional(String os){
        sendKeys(operationalSystem, os);
    }

    public void preencherVersaoSO(String version) {
        sendKeys(osBuild, version);
    }

    public void enviarPerfil() {
        click(sendProfile);
    }

    public void excluirPerfil() { click(exclude); }

    public void clicarEmApagarPerfil() { click(deleteProfile) ;}

    public void selecionarPerfil(String perfil) {
        comboBoxSelectByVisibleText(profileComboBox, perfil);
    }

    public boolean verificarSePerfilExiste(String perfil) {
        try
        {
            By profileOption = By.xpath("//option[text()='"+perfil+"']");
            waitForElement(profileOption);
            return true;
        }
        catch (WebDriverException e)
        {
            return false;
        }
    }
}
