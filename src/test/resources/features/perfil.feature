Feature: Perfil

  Background: Usuario logado
    Given estou logado

  Scenario: Criar um perfil com sucesso
    Given vou até 'minha conta'
    And entro em 'perfís'
    And preencho o formulário do perfil
    When envio o formulário do perfil
    Then o perfil deve ser criado com sucesso

  Scenario: Excluir um perfil com sucesso
    Given vou até 'minha conta'
    And entro em 'perfís'
    And seleciono o perfil
    And seleciono 'Apagar Perfil'
    When envio o formulário de excluir
    Then o perfil deve ser excluido com sucesso