package br.com.serenitybddtemplate.pages;

import br.com.serenitybddtemplate.bases.PageBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BugReportPage extends PageBase {

    //Constructor
    public BugReportPage(WebDriver driver){
        super(driver);
    }

    //Mapping
    By buglist = By.id("buglist");
    By categoryComboBox = By.name("category_id");
    By summaryField = By.name("summary");
    By descriptionField = By.name("description");
    By uploadFileField = By.id("ufile[]");
    By submitButton = By.xpath("//input[@value='Enviar Relatório']");
    By publicSelection = By.xpath("//input[@value='10']");
    By privateSelection = By.xpath("//input[@value='50']");
    By stepsToReproduceField = By.name("steps_to_reproduce");

    //Actions
    public void selecionarCategoria(String categoria){
        comboBoxSelectByVisibleText(categoryComboBox, categoria);
    }

    public void preencherResumo(String resumo){
        sendKeys(summaryField, resumo);
    }

    public void preencherDescricao(String descricao){
        sendKeys(descriptionField, descricao);
    }

    public void inserirAnexo(String caminhoArquivo){
        sendKeysWithoutWaitVisible(uploadFileField, caminhoArquivo);
    }

    public void preencherPassos(String steps){
        sendKeys(stepsToReproduceField, steps);
    }

    public void clicarEmSubmitReport(){
        click(submitButton);
    }

    public void selecionarVisibilidadePublica(){
        click(publicSelection);
    };

    public String receberLinkDoCasoEnviado()
    {
        By submittedCase = By.xpath("//a[contains(text(),'Visualizar Caso Enviado')]");
        String link = getLink(submittedCase);
        return link;
    }

    public void selecionarVisibilidadePrivada(){
        click(privateSelection);
    };

    public boolean retornaVerdadeiroPaginaDeCasos()
    {
        waitForElement(buglist);
        String url = getURL();
        if (url.equals("https://mantis-prova.base2.com.br/view_all_bug_page.php")) {
            return true;
        }
        return false;
    }
}
