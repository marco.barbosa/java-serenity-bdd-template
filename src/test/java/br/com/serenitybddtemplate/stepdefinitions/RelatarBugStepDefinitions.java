package br.com.serenitybddtemplate.stepdefinitions;

import br.com.serenitybddtemplate.steps.*;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;

public class RelatarBugStepDefinitions {
    @Steps
    LoginSteps loginSteps;

    @Steps
    MainSteps mainSteps;

    @Steps
    BugReportSteps bugReportSteps;

    @Given("aciono 'relatar caso'")
    public void clicarEmRelatarCaso(){
        mainSteps.clicarEmReportIssue();
    }

    @And("preencho o formulário do caso com visibilidade '(.*)'")
    public void preencherCaso(String visibilidade){
        String project = "[Todos os Projetos] 7EI2PODHPN";
        String frequency = "às vezes";
        String severity = "texto";
        String priority = "baixa";
        String profile = "Desktop  Windows 10";
        String resume = "Mensagem errada de login inválido.";
        String description = "Ao proceder o login com a senha incorreta aparece a mensagem: 'Your account may be disabled or blocked or the username/password you entered is incorrect'. A mensagem esperada era: 'Invalid username or password.'";
        String steps ="1. Acessar a tela de 'login'\r\n2. Preencher ‘Username’ com 'marco.barbosa'\r\n3. Preencher ‘Password’ com '12345678'\r\n4. Desmarcar [Secure Session]\r\n5. Acionar [Login]";

        bugReportSteps.selecionarCategoria(project);
        bugReportSteps.preencherResumo(resume);
        bugReportSteps.preencherDescricao(description);
        bugReportSteps.selecionarVisibilidade(visibilidade);
        bugReportSteps.preencherPassos(steps);
    }

    @When("enviar o formulário")
    public void clicarEnviarRelatorio(){ bugReportSteps.clicarEmSubmitReport(); }

    @Then("o caso deve ser criado com sucesso")
    public void verificaSeCasoFoiCriado(){
        String url = "https://mantis-prova.base2.com.br";
        int urlLength = url.length();
        String casoEnviadoLinkCompleto = bugReportSteps.receberLinkDoCasoEnviado();
        boolean aguardarRetornoPaginaDeCasos = bugReportSteps.retornoPaginaDeCasos();
        String casoEnviadoLinkReduzido = casoEnviadoLinkCompleto.substring(urlLength);
        Assert.assertEquals(mainSteps.verificaSeLinkExiste(casoEnviadoLinkReduzido), true);
    }
}
