package br.com.serenitybddtemplate.pages;

import br.com.serenitybddtemplate.bases.PageBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

public class AccountPage extends PageBase {
    //Constructor
    public AccountPage(WebDriver driver){
        super(driver);
    }
    //Mapping
    By accoutProfileMenu = By.xpath("//a[@href='/account_prof_menu_page.php']");

    //Actions
    public void clicarEmPerfis(){
        click(accoutProfileMenu);
    }
}
