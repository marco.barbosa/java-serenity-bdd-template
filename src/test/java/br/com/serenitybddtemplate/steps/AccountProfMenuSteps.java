package br.com.serenitybddtemplate.steps;

import br.com.serenitybddtemplate.pages.AccountProfMenuPage;
import net.thucydides.core.annotations.Step;

public class AccountProfMenuSteps {
    AccountProfMenuPage accountProfMenuPage;
    @Step("preencher plataforma")
    public void preencherPlataforma(String plataforma) { accountProfMenuPage.preencherPlataforma(plataforma); }

    @Step("preencher sistema operacional")
    public void preencherSistemaOperacional(String os) { accountProfMenuPage.preencherSistemaOperacional(os); }

    @Step("preencher versão sistema operacional")
    public void preencherVersaoSistemaOperacional(String version) { accountProfMenuPage.preencherVersaoSO(version); }

    @Step("clicar em 'Adicionar Perfil'")
    public void clicarEmAdicionarPerfil() { accountProfMenuPage.enviarPerfil(); }

    @Step("selecionar o perfil")
    public void selecionarPerfil (String perfil) { accountProfMenuPage.selecionarPerfil(perfil); }

    @Step("clicar em 'Apagar Perfil'")
    public void clicarEmApagarPerfil() { accountProfMenuPage.clicarEmApagarPerfil(); }

    @Step("excluir o perfil")
    public void excluirPerfil() { accountProfMenuPage.excluirPerfil(); }

    @Step("verificar se o perfil foi criado")
    public boolean verificarSePerfilExiste(String perfil) { return accountProfMenuPage.verificarSePerfilExiste(perfil); }
}
