package br.com.serenitybddtemplate.steps;

import br.com.serenitybddtemplate.pages.AccountPage;
import net.thucydides.core.annotations.Step;

public class AccountSteps {
    AccountPage accountPage;

    @Step("entro em 'perfís'")
    public void clicarEmPerfis() { accountPage.clicarEmPerfis(); }

}
