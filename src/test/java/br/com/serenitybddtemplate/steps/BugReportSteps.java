package br.com.serenitybddtemplate.steps;

import br.com.serenitybddtemplate.pages.BugReportPage;
import net.thucydides.core.annotations.Step;

public class BugReportSteps {

    BugReportPage bugReportPage;

    @Step("preencher categoria")
    public void selecionarCategoria(String categoria){
        bugReportPage.selecionarCategoria(categoria);
    }

    @Step("preencher resumo")
    public void preencherResumo(String resumo){
        bugReportPage.preencherResumo(resumo);
    }

    @Step("preencher descrição")
    public void preencherDescricao(String descricao){
        bugReportPage.preencherDescricao(descricao);
    }

    @Step("inserir anexo")
    public void inserirAnexo(String caminhoArquivo){
        bugReportPage.inserirAnexo(caminhoArquivo);
    }

    @Step("preencher passos")
    public void preencherPassos(String steps){ bugReportPage.preencherPassos(steps); }

    @Step("enviar relatório")
    public void clicarEmSubmitReport(){
        bugReportPage.clicarEmSubmitReport();
    }

    @Step("selecionar visibilidade")
    public void selecionarVisibilidade(String visibilidade){
       if(visibilidade.equalsIgnoreCase("público")){
            bugReportPage.selecionarVisibilidadePublica();
        } else {
           bugReportPage.selecionarVisibilidadePrivada();
       }
    }

    public String receberLinkDoCasoEnviado(){ return bugReportPage.receberLinkDoCasoEnviado(); }

    public Boolean retornoPaginaDeCasos(){ return bugReportPage.retornaVerdadeiroPaginaDeCasos(); }
}
