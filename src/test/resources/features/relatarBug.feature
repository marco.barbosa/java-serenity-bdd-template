Feature: Criar Bug

  Background: Usuario logado
    Given estou logado

  Scenario: Relatar um Bug público com sucesso
    Given aciono 'relatar caso'
    And preencho o formulário do caso com visibilidade 'público'
    When enviar o formulário
    Then o caso deve ser criado com sucesso